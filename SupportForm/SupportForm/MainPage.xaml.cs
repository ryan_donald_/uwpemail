﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Popups;
using Windows.ApplicationModel.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace SupportForm
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();
        }

        private async void button_Click_1(object sender, RoutedEventArgs e)
        {
            MessageDialog msgbox = new MessageDialog("Do you want to send Email on These Details ?\nTo : " + To.Text + " \nFrom : " + From.Text + " \nSubject : " + Subject.Text + "\nDescriptions : " + Descriptions.Text + "", "Suppport Form");
            msgbox.Commands.Clear();
            msgbox.Commands.Add(new UICommand { Label = "SUBMIT", Id = 0 });
            msgbox.Commands.Add(new UICommand { Label = "CANCEL", Id = 1 });

            var res = await msgbox.ShowAsync();

            if ((int)res.Id == 0)
            {
                MessageDialog msgbox2 = new MessageDialog("Email Sended ", "Information");
                await msgbox2.ShowAsync();
            }

            if ((int)res.Id == 1)
            {
                MessageDialog msgbox2 = new MessageDialog("Canceled Email", "Cancel");
                await msgbox2.ShowAsync();
            }

            if ((int)res.Id == 2)
            {
                MessageDialog msgbox2 = new MessageDialog("Nevermind then... :|", "User Response");
                await msgbox2.ShowAsync();
            }
        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {
            To.Text = "";
            From.Text = "";
            Subject.Text = "";
            Descriptions.Text = "";
        }

        private void button2_Click(object sender, RoutedEventArgs e)
        {
            CoreApplication.Exit();
        }
    }
}
